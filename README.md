# CAC-DXA

CAC-DXA implements the algorithm presented in:

Stukowski, A. (2014). A triangulation-based method to identify dislocations in atomistic models. Journal of the Mechanics and Physics of Solids, 70, 314-319.

to extract dislocations from atomistic or CAC models.
The output of the code is a vtk file which contains the mesh around the dislocation lines.
When the input is a CAC snapshot file, the code only analyzes the finite element regions. 

## Installation

Use python setuptools to install this module.
The command must be run from the base directory of the program.

```bash
cd path/to/cac-dxa
pip install -e .
```

