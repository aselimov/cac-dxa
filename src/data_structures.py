# %% Import required modules
import numpy as np
import skeletor as sk
import trimesh
import pygments

# %% Define the finite element class 
# This class represents each independent finite element
# The only information we currently need is the size of the element and a 3xnode_num array containing the 
# node positions
class Element:
    face_recipe = [[0,1,2,3],[0,1,5,4],[1,2,6,5],[0,3,7,4],[3,2,6,7],[4,5,6,7]]

    @staticmethod
    def get_face_recipe():
        return Element.face_recipe

    def __init__(self, node_positions, esize):
        self.nodes = node_positions
        self.node_delta=np.zeros((8,3))
        self.esize = esize
        self.slipped=[0 for i in range(8)]

    # Return node position for specific node
    def get_node_pos(self,node):
        return self.nodes[node,:]

    def get_output_data(self):
        return zip(self.slipped, self.nodes)
    # Return all face centroids 
    def face_centroids(self):
        
        myface=list()
        for i,f in enumerate(self.face_recipe):
            center=np.zeros(3)
            for n in f:
                center += self.nodes[n,:]/4

            myface.append([i, np.array(center)])

        return myface
    
    def face_nodes(self, face_num):
        return self.nodes[[val for val in self.face_recipe[face_num]]]
        
    def set_slipped(self, face):
        for n in self.face_recipe[face]:
            self.slipped[n] = 1

    #This gives you the ideal neighbor vector in the unslipped configuration
    def get_ideal_neighbor_vector(self, node, face):
        nei_face=5-face 
        ind=self.face_recipe[face].index(node)
        return (self.nodes[self.face_recipe[face][ind], :] - self.nodes[self.face_recipe[nei_face][ind], :])/(self.esize-1)

    def shape_fun(self, i, j, k):
        shape=np.zeros(8)
        shape[0] = (1-i)*(1-j)*(1-k)/8
        shape[1] = (1+i)*(1-j)*(1-k)/8
        shape[2] = (1+i)*(1+j)*(1-k)/8
        shape[3] = (1-i)*(1+j)*(1-k)/8
        shape[4] = (1-i)*(1-j)*(1+k)/8
        shape[5] = (1+i)*(1-j)*(1+k)/8
        shape[6] = (1+i)*(1+j)*(1+k)/8
        shape[7] = (1-i)*(1+j)*(1+k)/8

        return shape
    
    def interpolate_face(self, face, for_ovito=False):

        if for_ovito:
            layers=3
        else:
            layers=1
        # These are the bounds for the interpolation loop. To interpolate atoms at one face we just have to fix one of the natural coordinates
        if face==0:
            iklo, ikhi, ijlo, ijhi, iilo, iihi = 0, layers, 0, self.esize, 0, self.esize

        elif face==1:
            iklo, ikhi, ijlo, ijhi, iilo, iihi = 0, self.esize, 0, layers, 0, self.esize

        elif face==2:
            iklo, ikhi, ijlo, ijhi, iilo, iihi = 0, self.esize, 0, self.esize, self.esize-layers, self.esize

        elif face==3:
            iklo, ikhi, ijlo, ijhi, iilo, iihi = 0, self.esize, 0, self.esize, 0, layers

        elif face==4:
            iklo, ikhi, ijlo, ijhi, iilo, iihi = 0, self.esize, self.esize-layers, self.esize, 0, self.esize

        elif face==5:
            iklo, ikhi, ijlo, ijhi, iilo, iihi = self.esize-layers, self.esize, 0, self.esize, 0, self.esize

        atoms = list()
        for ik in range(iklo, ikhi):
            k=-1.0 + ik*(2.0/(self.esize-1))
            for ij in range(ijlo, ijhi):
                j=-1.0 + ij*(2.0/(self.esize-1))
                for ii in range(iilo, iihi):
                    i=-1.0 + ii*(2.0/(self.esize-1))
                    
                    shape_fun=self.shape_fun(i,j,k)
                    r=np.zeros(3) 
                    for node, shape in zip(self.nodes,shape_fun):
                        r+= node*shape
                    
                    atoms.append(r)

                    #Figure out if it's an edge atom too

        return atoms

    def displace_node(self, n, delta):
        self.node_delta[n, :] += delta
        self.nodes[n, :] += delta
        return
    
    def reset_nodes(self):
        for node, delta in zip(self.nodes,self.node_delta):
            node-=delta
        return
    
    def rotation_matrix(self):
        # This returns the rotation matrix required to transform the finite element from the unrotated [100],[010],[001] orientation
        # to the current configuration

        #These are the reference orientation vectors
        x1 = np.array([1.0,1.0,0.0])
        x1 /= np.linalg.norm(x1)
        y1 = np.array([0.0,1.0,1.0])
        y1 /= np.linalg.norm(y1)
        z1 = np.array([1.0,0.0,1.0])
        z1 /= np.linalg.norm(z1)

        #Vectors in the current configuration
        x2 = self.nodes[1,:]-self.nodes[0,:]
        x2 = x2/np.linalg.norm(x2)
        y2 = self.nodes[3,:]-self.nodes[0,:]
        y2 = y2/np.linalg.norm(y2)
        z2 = self.nodes[4,:]-self.nodes[0,:]
        z2 = z2/np.linalg.norm(z2)

        ori1=np.array([x1, y1, z1])
        ori2=np.array([x2, y2, z2])
        return np.matmul(np.linalg.inv(ori1), ori2)

# %% Define the dislocation network class which takes the slipped triangle mesh and generates dislocation points
class DislocationNetwork:
    def __init__(self, tri, points):
        self.skeletonize_dislocation_network(tri,points)   
        self.print_length()
    
    def skeletonize_dislocation_network(self, tri, points):
        #We take as an input the the list of indices which form the tetrahedra and the list of vertices. 
        #Critically the tetrahedra function also contains the burgers vector

        # First calculate a list of all triangle face centers
        mesh=trimesh.Trimesh(vertices=points, faces=[val[0:3] for val in tri])
        fixed=sk.pre.fix_mesh(mesh, remove_disconnected=5)
        cont=sk.pre.contract(fixed,epsilon=0.05, operator='umbrella', SL=2, progress=False)
        #self.skel=sk.skeletonize.by_vertex_clusters(cont, sampling_dist=5, progress=False, cluster_pos='center')
        self.skel=sk.skeletonize.by_wavefront(cont, step_size=3, progress=False)
        self.skel.mesh=fixed
        sk.post.clean_up(self.skel,inplace=True, theta=0.8)
        sk.post.radii(self.skel, method='knn')
        #self.skel.show(mesh=True)

    # Print the total dislocation length calculated from the skeletonization procedure
    def print_length(self):
        length=0
        for v1,v2 in self.skel.edges:
            length+= np.linalg.norm(np.array(self.skel.vertices[v2])-np.array(self.skel.vertices[v1]))
        print(length)

    def viz_structure(self):
        self.skel.show(mesh=True)


