import numpy as np
from data_structures import Element
from pathlib import Path
import os

# %% Define the reader class
# The reader class primarily handles reading in the input files and contains the element lists
class Reader:
    def __init__(self, filename):
        self.infile=filename
        self.read_file(filename)

    #Method for reading in data from a .out formatted file which is output from the CAC version
    def read_file(self,filename):
        self.elements=list()
        with open(filename, 'r') as dat:
            self.elements=list()
            i = 0
            while(True): 
                line = dat.readline()

                #Check for EOF
                if line == '':
                    break
                else:
                    line = line.split()

                #Read in number of atoms and elements
                if i == 3:
                    atom_num = int(line[1])
                    ele_num = int(line[3])

                #Skip atomistic section
                if i == 7:
                    for j in range(atom_num):
                        line = dat.readline().split()

                #Read in the CG region
                if i == 8:
                    for ie in range(ele_num):
                        line = dat.readline().split()
                        esize=int(line[3])
                        #Initialize nodal variables
                        nodepos = np.zeros((8,3))

                        #Read in all of the nodes
                        for node in range(8):
                            line = dat.readline().split()
                            try: 
                                for j in range(3):
                                    nodepos[node,j] = float(line[3+j])
                            except IndexError:
                                print(line)

                        #Add the data to the element data object
                        self.elements.append(Element(nodepos, esize+1))
                i+=1
        return

    def get_elements(self):
        return self.elements
    
    def get_infile(self, base=False, prefix=''):
        if base:
            p=Path(self.infile)
            return str(p.parent)+os.sep+prefix+p.with_suffix('').name
        else:
            return self.infile

class Writer:
    def __init__(self):
        pass

    #Write out the slipped information to a file 
    def write_xyz(self, elements, outfile='dislocated_nodes.xyz'):

        with open(outfile, 'w') as f:
            nump = len(elements)*8
            f.write("{}\n-------\n".format(nump))
            for i,ele in enumerate(elements):
                for slip_status, node in ele.get_output_data(): 
                    f.write("{} {} {} {} {}\n".format(i, slip_status, *node))
        return

    def write_disregistry_xyz(self, atoms, outfile='disregistry.xyz'):
        with open(outfile, 'w') as f:
            f.write("{}\n-------\n".format(len(atoms)))
            for atom in atoms:
                f.write("{} {} {} {}\n".format(np.linalg.norm(atom[3:]), *atom[0:3]))

    def write_atomistic_dxa(self, tetrahedra, points, outfile='atom_dxa.xyz'):
        with open(outfile, 'w') as f:
            f.write("{}\n-------\n".format(len(tetrahedra)))
            for v1, v2, v3, mapl, bx, by, bz in tetrahedra:
                f.write("{} {} {} {} {} \n".format(mapl, *(points[v1]+points[v2]+points[v3])/3, np.linalg.norm([bx, by, bz])))

    def write_atoms_xyz(self, atoms, outfile='atoms.xyz'):
        with open(outfile, 'w') as f:
            f.write("{}\n-------\n".format(len(atoms)))
            for x,y,z in atoms:
                f.write("{} {} {}\n".format(x,y,z))

    
    def write_dxa_vtk(self, vertices, points, outfile='dislocation_line.vtk'):
        # Vertices contains lists of indices which index into points list while points list
        # contains all of the atoms which form the dislocated element faces
        #Vertices are indices which form tetrahedra that are dislocated

        #First get a list of the unique vertex points, map the original index to the new index
        unique_indices= np.unique(np.array([ind for tetrahedra in vertices for ind in tetrahedra[0:3]]))
        index_map={key:value for value,key in enumerate(unique_indices)}
        unique_points=[points[ind] for ind in unique_indices]

        with open(outfile, 'w') as f:

            # First write the vtk header information
            f.write('# vtk DataFile Version 4.0.1\n')
            f.write('Dislocation line analyzed via cac analysis\n')
            f.write('ASCII\n')
            f.write('DATASET UNSTRUCTURED_GRID\n')

            #Now write the points
            f.write('POINTS {} float\n'.format(len(unique_points)))
            for point in unique_points:
                f.write('{} {} {}\n'.format(*point))
            
            #Now write the tetrahedral cells
            f.write('CELLS {} {}\n'.format(len(vertices), len(vertices)*4))
            for tetrahedron in vertices:
                f.write('3 {} {} {}\n'.format(*[index_map[key] for key in tetrahedron[0:3]]))
            
            #Now write the cell types, 5 is for triangle and 10 is for tetrahedra
            f.write('CELL_TYPES {}\n'.format(len(vertices)))
            for i in range(len(vertices)):
                f.write('5\n')
