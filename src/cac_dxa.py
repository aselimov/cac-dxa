#This script is for testing  prototyping the CAC dislocation analysis algorithm

# %% First we do the initial setup

import numpy as np
import scipy
import sys
from scipy.spatial import Delaunay
from scipy.spatial import KDTree
import os
from collections import Counter
import math

from graph import Graph
from data_structures import Element, DislocationNetwork
from constants import INT_HUGE
from reader_writer import Reader,Writer

# %% Now initialize the CAC DXA class
class CacDxa:
    # This requires an initialized reader object in the constructor
    def __init__(self, reader):
        # Perform the CAC-DXA 
        self.slipped_face_atoms=list()
        self.for_atomistic_dxa=list()
        self.for_ovito=list()
        self.cac_dxa(reader.get_elements())

        # Now output the slipped face atoms to an xyz file
        if len(self.slipped_face_atoms) > 0:
            self.dislocated_facets=list()
            self.set_lattice_vector_orientation(reader.get_elements()[0])
            self.atomistic_dxa()
            self.disl=DislocationNetwork(self.dislocated_tetrahedra, self.for_atomistic_dxa)
        
        writer=Writer()

        #Output datafiles
        writer.write_xyz(reader.get_elements(), outfile=reader.get_infile(prefix='nodes_',base=True)+'.xyz')
        writer.write_disregistry_xyz(self.slipped_face_atoms, outfile=reader.get_infile(prefix='disregistry_',base=True)+'.xyz')
        writer.write_atoms_xyz(self.for_ovito, outfile=reader.get_infile(prefix='ovito_',base=True)+'.xyz')
        writer.write_dxa_vtk(self.dislocated_facets, self.for_atomistic_dxa, outfile=reader.get_infile(prefix='dxa_',base=True)+'.vtk')

    #The cac_dxa method uses the node-neighbor vectors to calculate the 
    def cac_dxa(self, elements):

        face_recipe=Element.get_face_recipe()
        self.faces=list()
        # First we have to build a list of all face centroids so we can determine nearest neighbor faces
        # To do this we create self.faces which is a list of lists with each list containing the face_type, face_centroid, element_index
        for i,e in enumerate(elements):
            element_faces=e.face_centroids()
            for l in element_faces:
                l.append(i)
            self.faces.extend(element_faces)

        # Now create the point cloud with all finite element centroids
        tree = scipy.spatial.KDTree(np.array([face[1] for face in self.faces]))

        #Now loop over all faces
        for ind, face in enumerate(self.faces):
            #First figure out the nearest neighbor faces
            nei_ind = tree.query(face[1],k=2)[1][1]
            neighbor=self.faces[nei_ind]

            #Now make sure that this face is of the opposite type as original face otherwise we don't analyze it because it means
            #that the finite element is not adjacent to another finite element. We also only check if the face is first in the list
            if ((5-neighbor[0]) == face[0]) and (ind < nei_ind):

                ele=elements[face[2]]
                nei_ele=elements[neighbor[2]]

                # Calculate the distance between all nearest neighbor nodes between the neighboring finite element faces
                nei_vecs=list()
                for node, nei_node in zip(ele.face_nodes(face[0]), nei_ele.face_nodes(neighbor[0])):
                    nei_vecs.append(node-nei_node)
                #Now find the difference in the neighbor vectors for all permutations to get the maximum difference which is related to the 
                #slip vector of the dislocation
                diff=[(nv1, nv2, np.linalg.norm(v1-v2)) for nv1, v1 in enumerate(nei_vecs) for nv2, v2 in enumerate(nei_vecs)]
                
                #If the maximum difference in neighbor vector is above 0.5 than we consider that to result from a dislocation
                #0.5 is slightly arbitrary, however it does a good job to filter out small differences in the neighbor vectors that may occur
                #due to fluctuations.
                if max([val[2] for val in diff]) > 0.75:
                    ele.set_slipped(face[0])
                    nei_ele.set_slipped(neighbor[0])
                    
                    #Try some disregistry analysis

                    # First get the atoms on the element face in the current/slipped configuration
                    current_face_atoms =ele.interpolate_face(face[0])
                    current_neighbor_atoms =nei_ele.interpolate_face(neighbor[0])
                    
                    #Append the atoms for the atomistic analysis
                    self.for_atomistic_dxa+=current_face_atoms+current_neighbor_atoms
                    
                    # Append atoms for ovito analysis
                    self.for_ovito += ele.interpolate_face(face[0], for_ovito=True) + nei_ele.interpolate_face(neighbor[0], for_ovito=True)
                    #Displace all nodes on the face
                    for node, nei_node, face_node, nei_face_node in zip(ele.face_nodes(face[0]), nei_ele.face_nodes(neighbor[0]),face_recipe[face[0]], face_recipe[neighbor[0]]):
                        delta=(nei_node-node) - (ele.get_ideal_neighbor_vector(face_node, face[0]))
                        ele.displace_node(face_node, delta)
                        nei_ele.displace_node(nei_face_node, delta)

                    ref_face_atoms = ele.interpolate_face(face[0])
                    ref_neighbor_atoms = nei_ele.interpolate_face(neighbor[0])
                    
                    
                    for cur_atom, ref_atom in zip(current_face_atoms, ref_face_atoms):
                        dis=cur_atom-ref_atom
                        self.slipped_face_atoms.append((*cur_atom, *dis))
                   
                    for cur_atom, ref_atom in zip(current_neighbor_atoms, ref_neighbor_atoms):
                        dis=cur_atom-ref_atom
                        self.slipped_face_atoms.append((*cur_atom, *dis))

                    #reset nodes
                    ele.reset_nodes()
                    nei_ele.reset_nodes()
                    
    # This code applies the DXA algorithm from https://doi.org/10.1016/j.jmps.2014.06.009 to interpolated atoms on neighboring
    # finite element faces
    def atomistic_dxa(self):
    
        #Sadly we have to loop over the Delaunay triangulation twice. The first time we have to loop over to build a graph
        #to calculate the all paths of length two between the two vertexes.
        tri=Delaunay(self.for_atomistic_dxa)      
        adjacency=[list() for i in range(len(self.for_atomistic_dxa))]
        adj_mat=np.zeros((len(self.for_atomistic_dxa), len(self.for_atomistic_dxa)))
        for tet_num,(i1, i2, i3, i4) in enumerate(tri.simplices):
            #Loop over all triangles in the tetrahedron.
            for tri_num, (vertex1, vertex2, vertex3) in enumerate([[i1, i2, i3],[i2, i3, i4], [i3, i4, i1], [i4, i1, i2]]):
                #Now calculate the three edge vectors for the current triangle
                v1 = self.for_atomistic_dxa[vertex2] - self.for_atomistic_dxa[vertex1] 
                v2 = self.for_atomistic_dxa[vertex3] - self.for_atomistic_dxa[vertex2]
                v3 = self.for_atomistic_dxa[vertex1] - self.for_atomistic_dxa[vertex3]
                
                #Now append to the adjacency list we use an adjacency matrix in addition to the adjacency list 
                # To track which edges have already been added as some edges are shared between different tetrahedrons
                if not adj_mat[vertex1,vertex2]:
                    adj_mat[vertex1,vertex2]=1
                    adj_mat[vertex2,vertex1]=1
                    adjacency[vertex1].append(vertex2)
                    adjacency[vertex2].append(vertex1)

                if not adj_mat[vertex3,vertex2]:
                    adj_mat[vertex3,vertex2]=1
                    adj_mat[vertex2,vertex3]=1
                    adjacency[vertex3].append(vertex2)
                    adjacency[vertex2].append(vertex3)

                if not adj_mat[vertex3,vertex1]:
                    adj_mat[vertex3,vertex1]=1
                    adj_mat[vertex1,vertex3]=1
                    adjacency[vertex3].append(vertex1)
                    adjacency[vertex2].append(vertex3)

            
        #create the graph
        graph=Graph(adjacency)

        #Now we need to match each edge in the graph to a lattice vector
        graph.init_weights()
        for v1, connection_list in enumerate(graph.get_adjacency_list()):
            for v2 in connection_list:
                #Check to make sure we haven't already assigned it. Ensures we only calculate each edge once
                if graph.get_weight(v1, v2) == INT_HUGE:
                    distance=self.for_atomistic_dxa[v2]-self.for_atomistic_dxa[v1]

                    #Only add if it's within the cutoff 
                    if np.linalg.norm(distance) < 3.2:
                        calculated_lattice_vectors=[self.map_to_lattice_vector(distance)]
                        
                        #Now set the edge as the most repeated lattice vector 
                        counts=Counter(calculated_lattice_vectors)
                        graph.set_weight(v1,v2, counts.most_common(1)[0][0])

                        #Now set the weight of the reverse as the opposite
                        graph.set_weight(v2, v1, self.opposite_lattice_vec[counts.most_common(1)[0][0]])


        # Now loop over all triangular facets and calculate the burgers vector
        self.dislocated_tetrahedra=list()
        for i1, i2, i3, i4 in tri.simplices:
            dislocated=False
            for vertex1, vertex2, vertex3 in [[i1, i2, i3],[i2, i3, i4], [i3, i4, i1], [i4, i1, i2]]:
                #Loop over the vertices which form each edge in the triangle
                l1=int(graph.get_weight(vertex1, vertex2))
                l2=int(graph.get_weight(vertex2, vertex3))
                l3=int(graph.get_weight(vertex3, vertex1))
                #Check to make sure they aren't all the same
                counts=Counter([l1, l2, l3])
                if any([val== INT_HUGE for val in [l1, l2, l3]]):
                    continue

                else:
                    burgers_vec=sum([self.lattice_vectors[vec]for vec in [l1, l2, l3]])
                    if np.linalg.norm(burgers_vec) > 0.8:
                        dislocated=True
                        self.dislocated_facets.append([ vertex1, vertex2, vertex3, self.map_to_lattice_vector(burgers_vec), *burgers_vec])
            if dislocated:
                for face in [[i1, i2, i3],[i2, i3, i4], [i3, i4, i1], [i4, i1, i2]]:
                    self.dislocated_tetrahedra.append(face)

    #This rotates the fcc lattice vectors to the orientation of an element. We assume all finite elements have the same orienation
    def set_lattice_vector_orientation(self, element):
        lattice_vectors=[                           
                           [0.5, 0.5, 0.0], [0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [-0.5, -0.5, 0.0], [0.0, -0.5, -0.5], [-0.5, 0.0, -0.5],
                          [-0.5, 0.5, 0.0], [0.0, -0.5, 0.5], [-0.5, 0.0, 0.5], [0.5, -0.5, 0.0], [0.0, 0.5, -0.5], [0.5, 0.0, -0.5], 
                          [0.3333333333333, 0.1666666666666, 0.1666666666666], [0.1666666666666, 0.3333333333333, 0.1666666666666],
                          [0.1666666666666, 0.1666666666666, 0.3333333333333], [-0.3333333333333, 0.1666666666666, 0.1666666666666],
                          [-0.1666666666666, 0.3333333333333, 0.1666666666666], [-0.1666666666666, 0.1666666666666, 0.3333333333333],
                          [0.3333333333333, -0.1666666666666, 0.1666666666666], [0.1666666666666, -0.3333333333333, 0.1666666666666],
                          [0.1666666666666, -0.1666666666666, 0.3333333333333], [0.3333333333333, 0.1666666666666, -0.1666666666666],
                          [0.1666666666666, 0.3333333333333, -0.1666666666666], [0.1666666666666, 0.1666666666666, -0.3333333333333],
                          [-0.3333333333333, -0.1666666666666, 0.1666666666666], [-0.1666666666666, -0.3333333333333, 0.1666666666666],
                          [-0.1666666666666, -0.1666666666666, 0.3333333333333], [0.3333333333333, -0.1666666666666, -0.1666666666666],
                          [0.1666666666666, -0.3333333333333, -0.1666666666666], [0.1666666666666, -0.1666666666666, -0.3333333333333],
                          [-0.3333333333333, 0.1666666666666, -0.1666666666666], [-0.1666666666666, 0.3333333333333, -0.1666666666666],
                          [-0.1666666666666, 0.1666666666666, -0.3333333333333], [-0.3333333333333, -0.1666666666666, -0.1666666666666],
                          [-0.1666666666666, -0.3333333333333, -0.1666666666666], [-0.1666666666666, -0.1666666666666, -0.3333333333333]
                        ]


        # Calculate the rotation matrix needed to transform from x = [100], y=[010], z=[001] to the current orientation. Assume it's the 
        # same for all finite elements
        rotation_matrix=element.rotation_matrix()
        inv_rotation=np.linalg.inv(rotation_matrix)

        lattice_vectors=lattice_vectors
        # Now transform the fcc lattice vectors to unit orientation vectors in the correct orientation
        self.lattice_vectors=[np.matmul(np.array(val)/np.linalg.norm(np.array(val)), rotation_matrix) for val in lattice_vectors]

        #Now create a list mapping each lattice vector to it's opposite
        self.opposite_lattice_vec=[self.map_to_lattice_vector(-vec) for vec in self.lattice_vectors]


    #This maps the vector vec to the nearest lattice_vector as previously defined by the finite element orientation
    def map_to_lattice_vector(self,vec):
        difference=[np.linalg.norm(lattice_vector - vec) for lattice_vector in self.lattice_vectors]
        return difference.index(min(difference))
    
    def viz_dislocation_structure(self):
        self.disl.viz_structure()
    
