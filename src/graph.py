# %% Import required files
import numpy as np
from constants import INT_HUGE

# %% Define an extremely basic undirected graph class that has weights.
class Graph:
    #Constructor takes in the adjacency list as an input
    def __init__(self, adjacency):
        self.adjacency=adjacency

    #This method returns all paths of length 2 starting at vertex1 and ending at vertex2
    def paths_len2(self, vertex1, vertex2):
        paths=list()
        for v in self.adjacency[vertex1]:
            if vertex2 in self.adjacency[v]:
                paths.append((vertex1, v, vertex2))
        
        return paths
    
    def init_weights(self):
        self.weights=np.zeros((len(self.adjacency),len(self.adjacency)))
        self.weights[:,:]=INT_HUGE
    
    def get_weight(self, vertex1, vertex2):
        return self.weights[vertex1, vertex2]
    
    def set_weight(self, vertex1, vertex2, weight):
        self.weights[vertex1,vertex2]=weight

    def get_adjacency_list(self):
        return self.adjacency
    
    #Return the minimum spanning tree for the graph. This generates multiple minimum spanning trees if points are seperated by edges larger
    # han 10
    def mst(self):
        #First create a flattened list of all edge lengths from the weight matrix
        weights=[(self.weights[i,j], i, j) for i in range(len(self.adjacency)) for j in self.adjacency[i]]
        weights.sort(key=lambda x: x[0])
        
        #We just need a list to keep track of the root node and the ranks of the sets
        root=[i for i,_ in enumerate(self.adjacency)]
        ranks=[0 for i,_ in enumerate(self.adjacency)]
        
        #Now loop until all edges have been added
        paths=list()
        i,e=0,0
        while e < len(self.adjacency)-1:
            if i == len(weights):
                break
            weight,v1,v2=weights[i]
            i+=1
            x=self.find(root,v1)
            y=self.find(root, v2)
            if x != y:
                e+=1
                paths.append([v1,v2])
                self.union(root, ranks, x, y)

        # Now for each path get the root node to distinguish distinct sets
        path_roots=list()
        for i,path in enumerate(paths):
            paths[i].append(self.find(root,path[0]))
            
        return paths


    def find(self, root, i):
        if root[i] == i:
            return i
        return self.find(root, root[i])
    
    def union(self, root, rank, x, y):
        xroot=self.find(root,x)
        yroot=self.find(root,y)
        if rank[xroot] < rank[yroot]:
            root[xroot] = yroot
        elif rank[xroot] > rank[yroot]:
            root[yroot] = xroot
        else:
            root[yroot] = xroot
            rank[xroot] +=1

    #Return all edges
    def all_edges(self):
        paths=list()
        for v1, adj in enumerate(self.adjacency):
            for v2 in adj:
                paths.append((v1,v2))
        return paths

