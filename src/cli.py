import argparse
from __init__ import __version__
from cac_dxa import CacDxa
from reader_writer import Reader

def main():
    parser = argparse.ArgumentParser(description='Analyze dislocations in CAC and atomistic models')
    parser.add_argument('infile', metavar='infile', type=str, nargs=1, help='Path to input file')
    parser.add_argument('-v', action='version', version='version: '+__version__)
    parser.add_argument('--viz', dest='viz', default=False, action='store_true', help='Visualize skeletonization of the dislocation network')
    args=parser.parse_args()

    reader=Reader(args.infile[0])
    cac_dxa=CacDxa(reader)

    if args.viz:
        cac_dxa.viz_dislocation_structure()


